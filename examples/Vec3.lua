--- A vector of 3 components
-- @classmod Vec3

local class = require("class")

local Vec3
Vec3 = class.strict {
  x = class.NULL,
  y = class.NULL,
  z = class.NULL,

  __init = function(self, x, y, z)
    self.x = x or 0
    self.y = y or 0
    self.z = z or 0
  end,

  __mt = {
    __eq = function(self, other)
      return self.x == other.x and
        self.y == other.y and
        self.z == other.z
    end,

    __add = function(self, other)
      return Vec3(
        self.x + other.x,
        self.y + other.y,
        self.z + other.z
      )
    end,

    __sub = function(self, other)
      return Vec3(
        self.x - other.x,
        self.y - other.y,
        self.z - other.z
      )
    end,

    __mul = function(self, other)
      return Vec3(
        self.x * other.x,
        self.y * other.y,
        self.z * other.z
      )
    end,

    __div = function(self, other)
      return Vec3(
        self.x / other.x,
        self.y / other.y,
        self.z / other.z
      )
    end,

    __len = function(self)
      return math.sqrt(self.x ^ 2 + self.y ^ 2 + self.z ^ 2)
    end,

    __unm = function(self)
      return Vec3(
        -self.x,
        -self.y,
        -self.z
      )
    end,

    __tostring = function(self)
      return string.format("(%f, %f, %f)", self.x, self.y, self.z)
    end,
  },
}

return Vec3
