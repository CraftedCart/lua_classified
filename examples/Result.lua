--- The result type can either store a success or error value
-- @classmod Result

local class = require("class")

local Result
Result = class.strict {
  _value = class.NULL,
  _is_ok = false,

  --- Create a new `Result`
  --
  -- @tparam Result self
  -- @param value The stored success or error value
  -- @tparam bool is_ok Whether the stored value is a success value or an error value
  --
  -- @see Result.Ok
  -- @see Result.Err
  __init = function(self, value, is_ok)
    self._value = value
    self._is_ok = is_ok
  end,

  --- Return the success value, or raise an error if the result stores an error value
  --
  -- @tparam Result self
  --
  -- @raise "Tried to unwrap an Err result"
  unwrap = function(self)
    assert(self._is_ok, "Tried to unwrap an Err result")
    return self._value
  end,

  --- Return the error value, or raise an error if the result stores a success value
  --
  -- @tparam Result self
  --
  -- @raise "Tried to unwrap an Ok result"
  unwrap_err = function(self)
    assert(not self._is_ok, "Tried to unwrap_err an Ok result")
    return self._value
  end,

  --- Return whether the result stores a success value
  --
  -- @tparam Result self
  is_ok = function(self) return self._is_ok end,

  --- Return whether the result stores an error value
  --
  -- @tparam Result self
  is_err = function(self) return not self._is_ok end,

  --- Return the success value, or raise an error with the given message if the result stores a error value
  --
  -- @tparam Result self
  -- @tparam string msg The message to display if the result `is_err`
  --
  -- @raise msg when the result is err
  expect = function(self, msg)
    assert(self._is_ok, msg)
    return self._value
  end,

  --- Return the error value, or raise an error with the given message if the result stores a success value
  --
  -- @tparam Result self
  -- @tparam string msg The message to display if the result `is_ok`
  --
  -- @raise msg when the result is ok
  expect_err = function(self, msg)
    assert(not self._is_ok, msg)
    return self._value
  end,
}

--- Create a new result with a success value
--
-- @param success_value The value to store in the result
Result.Ok = function(success_value)
  return Result(success_value, true)
end

--- Create a new result with an error value
--
-- @param error_value The value to store in the result
Result.Err = function(error_value)
  return Result(error_value, false)
end

return Result
