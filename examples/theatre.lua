local class = require("class")

local Actor
Actor = class {
  __init = function(self, name)
    self.name = name
  end,

  enter_stage = function(self)
    print("*" .. self.name .. "\t\tenters the stage*")
  end,

  leave_stage = function(self)
    print("*" .. self.name .. "\t\tleaves the stage*")
  end,

  dance = function(self, adjective)
    print("*" .. self.name .. "\t\tdances with " .. adjective .. "*")
  end,

  speak = function(self, msg)
    print(self.name .. ":\t\t" .. msg)
  end,
}

-- Bea inherits from Actor
local Bea
Bea = class(Actor) {
  __init = function(self)
    Actor.__init(self, "Bea") -- Call super __init
  end,

  leave_stage = function(self)
    -- Bea leaves the stage in style
    self:dance("style")
    Actor.leave_stage(self)
  end,
}

local Cleaner
Cleaner = class {
  clean_stage = function(self)
    print("The stage has been cleaned")
  end,
}

-- Travis is both a cleaner and an actor
-- Multiple inheritance!
local Travis
Travis = class(Actor, Cleaner) {
  __init = function(self)
    Actor.__init(self, "Travis")
  end,

  speak = function(self, msg)
    print(self.name .. " (loud):\t" .. msg)
  end,
}

-- Ok, let's act out a play
local bea = Bea()
local travis = Travis()
local ferris = Actor("Ferris")

travis:clean_stage()
bea:enter_stage()
travis:enter_stage()
travis:speak("I challenge thee to a dance-off!")
bea:speak("Challenge accepted. Bring. It. On.")
travis:dance("passion")
ferris:enter_stage()
travis:dance("fury")
bea:dance("passion")
bea:dance("elegance")
bea:dance("frenzy")
travis:speak("As you can see my dancing excellence clearly outmatches yours")
bea:speak("Who said you get to be the judge?")
travis:speak("Can't you see tha...")
bea:speak("Hey - you there! What's your name?")
ferris:speak("F...Ferris")
bea:speak("Who do you think had the smoother moves?")
ferris:speak("y.you")
travis:speak("Wh...")
travis:speak("Maam are you blind or something!? Couldn't you see my...")
bea:speak("Right, I'm out of here")
bea:leave_stage()
travis:speak("Hhmph")
travis:leave_stage()
ferris:leave_stage()
travis:clean_stage()

-- Output
-- ------
-- The stage has been cleaned
-- *Bea            enters the stage*
-- *Travis         enters the stage*
-- Travis (loud):  I challenge thee to a dance-off!
-- Bea:            Challenge accepted. Bring. It. On.
-- *Travis         dances with passion*
-- *Ferris         enters the stage*
-- *Travis         dances with fury*
-- *Bea            dances with passion*
-- *Bea            dances with elegance*
-- *Bea            dances with frenzy*
-- Travis (loud):  As you can see my dancing excellence clearly outmatches yours
-- Bea:            Who said you get to be the judge?
-- Travis (loud):  Can't you see tha...
-- Bea:            Hey - you there! What's your name?
-- Ferris:         F...Ferris
-- Bea:            Who do you think had the smoother moves?
-- Ferris:         y.you
-- Travis (loud):  Wh...
-- Travis (loud):  Maam are you blind or something!? Couldn't you see my...
-- Bea:            Right, I'm out of here
-- *Bea            dances with style*
-- *Bea            leaves the stage*
-- Travis (loud):  Hhmph
-- *Travis         leaves the stage*
-- *Ferris         leaves the stage*
-- The stage has been cleaned
